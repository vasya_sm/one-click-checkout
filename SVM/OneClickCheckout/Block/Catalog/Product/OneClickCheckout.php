<?php

namespace SVM\OneClickCheckout\Block\Catalog\Product;

class OneClickCheckout extends \Magento\Framework\View\Element\Template
{
	protected $_helper;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		array $data = [],
		\SVM\OneClickCheckout\Helper\Data $helper
	) {
		parent::__construct($context, $data);

		$this->_helper = $helper;
	}

	protected function _toHtml()
	{
//		var_dump($this->_scopeConfig->getValue(
//			'trans_email/ident_sales/email',
//			\Magento\Store\Model\ScopeInterface::SCOPE_STORE
//		));exit;
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');

		if ($this->_helper->getEnable() && !$customerSession->isLoggedIn()){
			return parent::_toHtml();
		}
		else {
			return '';
		}
	}
}