<?php

namespace SVM\OneClickCheckout\Block\Adminhtml\Reserved;

class Grid extends \Magento\Backend\Block\Widget\Grid\Container
{
	protected function _construct()
	{
		$this->_blockGroup = 'SVM_OneClickCheckout';
		$this->_controller = 'adminhtml_reserved';
		$this->_headerText = __('Reserved products');
		parent::_construct();
	}

}
