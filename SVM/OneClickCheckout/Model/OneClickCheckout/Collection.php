<?php

namespace SVM\OneClickCheckout\Model\OneClickCheckout;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('SVM\OneClickCheckout\Model\OneClickCheckout', 'SVM\OneClickCheckout\Model\ResourceModel\OneClickCheckout');
	}
}