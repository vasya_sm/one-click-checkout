<?php
namespace SVM\OneClickCheckout\Model\ResourceModel;

class OneClickCheckout extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	/**
	 * Model Initialization
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('svm_oneclickcheckout', 'id');
	}
}