<?php
namespace SVM\OneClickCheckout\Model;

class OneClickCheckout extends \Magento\Framework\Model\AbstractModel
{
	/**
	 * Model Initialization
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('SVM\OneClickCheckout\Model\ResourceModel\OneClickCheckout');
	}

	public function saveData($data){
		if(!empty($data)){
			$this->setData($data);
			$this->save();
		}
	}
}