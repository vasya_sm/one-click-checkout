<?php

namespace SVM\OneClickCheckout\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	protected $_scopeConfig;

	CONST ENABLE      = 'svm_oneclickcheckout/general/enable';
	CONST NOTIFICATION      = 'svm_oneclickcheckout/general/notification';

	public function __construct(
		\Magento\Framework\App\Helper\Context $context,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	) {
		parent::__construct($context);

		$this->_scopeConfig = $scopeConfig;
	}

	public function getEnable(){
		return $this->_scopeConfig->getValue(self::ENABLE);
	}

	public function getNotification(){
		return $this->_scopeConfig->getValue(self::NOTIFICATION);
	}

}

