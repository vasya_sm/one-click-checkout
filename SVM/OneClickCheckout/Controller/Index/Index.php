<?php
namespace SVM\OneClickCheckout\Controller\Index;


class Index extends \Magento\Framework\App\Action\Action
{
	protected $_storeManager;
	protected $inlineTranslation;
	protected $_helper;
	protected $_transportBuilder;
	protected $_scopeConfig;

	/**
	 * Index constructor.
	 *
	 * @param \Magento\Framework\App\Action\Context $context
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \SVM\OneClickCheckout\Helper\Data $helper
	 * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
	 */
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Store\Model\StoreManagerInterface  $storeManager,
		\SVM\OneClickCheckout\Helper\Data $helper,
		\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
	) {
		parent::__construct($context);
		$this->_helper = $helper;
		$this->inlineTranslation = $inlineTranslation;
		$this->_storeManager =$storeManager;
		$this->_transportBuilder = $transportBuilder;
		$this->_scopeConfig = $scopeConfig;
	}

	/**
	 * Default customer account page
	 * @return \Magento\Framework\View\Result\Page
	 */
	public function execute()
	{
		$post = $this->getRequest()->getPostValue();
		if(empty($this->_prepareData($post))){
			$this->_redirect($this->_redirect->getRefererUrl());
			return;
		}
		$post['date'] = new \DateTime('now');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$oneClickCheckoutModel = $objectManager->create('SVM\OneClickCheckout\Model\OneClickCheckout');
		$oneClickCheckoutModel->saveData($post);

		if($this->_helper->getNotification()){
			$templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID);
			$templateVars = array(
				'store' => $this->_storeManager->getStore(),
				'name' => $post['name'],
				'phone_number'   => $post['phone_number'],
				'product_id'   => $post['product_id']
			);
			$sender = [
				'name' => 'shop_owner',
				'email' => $this->_scopeConfig->getValue('trans_email/ident_sales/email'),
			];
			$to = [
				'name' => 'shop_owner',
				'email' => $this->_scopeConfig->getValue('trans_email/ident_sales/email'),
			];
			$this->inlineTranslation->suspend();
			$transport = $this->_transportBuilder->setTemplateIdentifier('oneclickcheckout_template')
				->setTemplateOptions($templateOptions)
				->setTemplateVars($templateVars)
				->setFrom($sender)
				->addTo($to)
				->getTransport();
			$transport->sendMessage();
			$this->inlineTranslation->resume();

		}
		$this->messageManager->addSuccess(
		__('Thanks for bought our product')
		);
		$this->_redirect($this->_redirect->getRefererUrl());
		return;

	}

	/**
	 *
	 * @param $data
	 * @return array
	 */
	protected function _prepareData($data){
		$preparedData = array();

		foreach($data as $key => $value){
			$preparedData[$key] = trim(strip_tags($value));
		}
		return $preparedData;
	}

}
?>