<?php

namespace SVM\OneClickCheckout\Controller\Adminhtml\Reserved;
use Magento\Framework\Controller\ResultFactory;

class Index extends \SVM\OneClickCheckout\Controller\Adminhtml\Reserved
{
	public function execute()
	{
		$resultPage = $this->_initAction();
		$resultPage->getConfig()->getTitle()->prepend(__('Reserved products'));
		return $resultPage;
	}
}